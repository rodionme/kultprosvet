export default {
  getBooks(query) {
    return fetch(
      `https://www.googleapis.com/books/v1/volumes?${new URLSearchParams({
        q: query,
      })}`,
    )
      .then(resp => resp.json())
      .catch(error => {
        throw new Error(error);
      });
  },
};
