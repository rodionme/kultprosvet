export const FETCH_BOOKS = 'fetchBooks';
export const ADD_BOOK_TO_CART = 'addBookToCart';
export const ORDER_BOOK = 'orderBook';
