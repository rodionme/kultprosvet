import Vue from 'vue';
import Vuex from 'vuex';
import router from '@/router';
import { ADD_BOOK_TO_CART, FETCH_BOOKS, ORDER_BOOK } from '@/store/actionTypes';
import { SET_BOOKS, ADD_BOOK, CLEAR_CART } from '@/store/mutationTypes';
import BookService from '@/services/book.service';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    books: [],
    cart: [],
  },

  mutations: {
    [SET_BOOKS](state, books) {
      state.books = books;
    },

    [ADD_BOOK](state, book) {
      state.cart.push(book);
    },

    [CLEAR_CART](state) {
      state.cart = [];
    },
  },

  actions: {
    [FETCH_BOOKS]({ commit }, query) {
      BookService.getBooks(query)
        .then(resp => {
          commit(
            SET_BOOKS,
            resp.items.map(
              ({
                id,
                saleInfo: { retailPrice, saleability },
                volumeInfo: {
                  authors,
                  description,
                  imageLinks: { smallThumbnail },
                  publishedDate,
                  title,
                },
              }) => ({
                authors,
                description,
                id,
                price: saleability === 'FOR_SALE' ? retailPrice : { amount: 0 },
                publishedDate: publishedDate
                  ? new Date(publishedDate).toLocaleString('uk-UA', {
                      year: 'numeric',
                      month: 'numeric',
                      day: 'numeric',
                    })
                  : '',
                thumbnail: smallThumbnail,
                title,
              }),
            ),
          );
        })
        .catch(() => {
          router.replace('/404');
        });
    },

    [ADD_BOOK_TO_CART]({ commit }, book) {
      commit(ADD_BOOK, book);
    },

    [ORDER_BOOK]({ commit }) {
      commit(CLEAR_CART);
    },
  },

  modules: {},
});
