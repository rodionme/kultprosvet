export const SET_BOOKS = 'setBooks';
export const ADD_BOOK = 'addBook';
export const CLEAR_CART = 'clearCart';
