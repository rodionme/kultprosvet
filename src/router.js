import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home';
import Cart from '@/views/Cart';
import NotFound from '@/views/NotFound';

Vue.use(VueRouter);

const routes = [
  { path: '/', name: 'Home', component: Home },
  { path: '/cart', name: 'Cart', component: Cart },
  { path: '/404', name: 'page404', component: NotFound },
  { path: '*', redirect: { name: 'page404' } },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
